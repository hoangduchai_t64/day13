<!DOCTYPE html>
<html lang="en">
<?php
include('../day12/conection.php');
session_start();
$gender = array(0 => "Nữ", 1 => "Nam");
$departmentArray = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
$listDepartmentStudent = array("1" => "MAT", "2" => "MAT", "3" => "KDL", "4" => "KDL");
$listStudents = getListStudent();
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="list-student.css">
</head>

<body>
    <div class="list-student-content">
        <?php
        function searchByFuculy($fuculy)
        {
            global $listStudents;
            global $departmentArray;

            $students = array();
            if (strcmp($fuculy, "EMPTY") != 0) {
                foreach ($listStudents as $key => $student) {
                    $keyDepartment = $student[3];
                    if (strcmp($keyDepartment, $fuculy) == 0) {
                        $students[] = $student;
                    }
                }
                $listStudents = $students;
            }
        }
        function searchByKeyword($keyWord)
        {
            global $listStudents;
            if (!empty(trim($keyWord))) {

                $students = array();
                foreach ($listStudents as $key => $student) {
                    if (strcmp(strtolower($student[1]), strtolower($keyWord)) == 0) {
                        $students[] = $student;
                    } elseif (strcmp(strtolower($student[5]), strtolower($keyWord)) == 0) {
                        $students[] = $student;
                    }
                }
                $listStudents = $students;
            }
        }
        if (isset($_POST['search'])) {
            if (isset($_POST['fuculy'])) {
                $_SESSION['fuculy'] = $_POST['fuculy'];
            }
            if (isset($_POST['keyWord'])) {
                $_SESSION['keyWord'] = $_POST['keyWord'];
            }
            searchByFuculy($_POST['fuculy']);
            searchByKeyword($_POST['keyWord']);
        }
        ?>
        <div class="table-1">
            <form method="POST">
                <table id="table-content1">


                    <tr>

                        <td>
                            <label>Khoa</label>
                        </td>
                        <td>
                            <select name="fuculy" id="select-item">
                                <?php
                                foreach ($departmentArray as $key => $value) {
                                    echo "<option  value='$key'";
                                    if (isset($_SESSION['fuculy']) && $_SESSION['fuculy'] == $key) {
                                        echo " selected";
                                    }
                                    echo ">$value</option>";
                                }
                                ?>
                    </tr>
                    <tr>
                        <td><label>Từ khóa</label></td>
                        <td> <input type='text' id='keyWord' name="keyWord" value="<?php if (isset($_SESSION['keyWord'])) {
                                                                                        echo $_SESSION['keyWord'];
                                                                                    }
                                                                                    echo ""; ?>" /></td>
                    </tr>

                </table>
                <div id="btn-search">
                    <input id="btn-search" type='submit' class='btn-box' value="Tìm kiếm" name="search">
                </div>
            </form>
        </div>



        <div class="table-2">
            <table id="table-content2">
                <tr class="label-content">
                    <th class="no">Id</th>
                    <th class="name-student">Name</th>
                    <th>Gender</th>
                    <th class="department">Fuculty</th>
                    <th>Birthday</th>
                    <th>Address</th>
                    <th>Avartar</th>
                </tr>
                <?php
                foreach ($listStudents as $key => $student) {

                    $keyGender = $student[2];
                    $keyDepartment = $student[3];
                    echo ("  
                             <tr>
                                <td class=\"no\">$student[0]</td>
                                <td class=\"name-student\">$student[1]</td>
                                <td class=\"department\">$gender[$keyGender]</td>
                                <td class=\"department\">$departmentArray[$keyDepartment]</td>
                                <td class=\"department\">$student[4]</td>
                                <td class=\"department\">$student[5]</td>
                                <td class=\"department\"><img src= \"$student[6]\" width=\"100\" height=\"50\" ></td>
                             </tr>
                         ");
                }
                ?>

            </table>
        </div>
    </div>

</body>

</html>