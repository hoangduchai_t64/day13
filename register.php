<?php
include("../day12/conection.php");
$genderArray = array(0 => "Nữ", 1 => "Nam");
$departmentArray = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
date_default_timezone_set('Asia/Ho_Chi_Minh');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="register.css">


</head>

<body>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $("#datepicker").datepicker({
                dateFormat: "dd/mm/yy"
            }).val()
        });
    </script>



    <div class='login-content'>
        <div id="label-infor">
       
            <label class="label">
                <?php
                function is_image()
                {
                    if ($_FILES["image"]["error"] === UPLOAD_ERR_OK) {

                        $check = getimagesize($_FILES["image"]["tmp_name"]);
                        if (!$check) {
                            echo "Tệp được chọn phải là tệp ảnh";
                            return false;
                        }
                    }

                    return true;
                }

                function uploadImage()
                {
                    if (!file_exists('upload')) {
                        mkdir('upload', 0777, true);
                    }
                    session_start();
                    if (!empty($_FILES["image"]["name"])) {
                        $_SESSION = $_POST;
                        $sourceParth = $_FILES["image"]["tmp_name"];
                        $fileName = $_FILES["image"]["name"];
                        $target_dir = "upload/";
                        $arrayInfor = explode(".", $fileName);  
                        $extension = $arrayInfor[1];
                        $name = $arrayInfor[0];
                        $target_file  = $target_dir . $name . "_" . date("YmdHis") . "." . $extension;

                        if (move_uploaded_file($sourceParth, $target_file)) {
                            $_SESSION["image-upload"] = $target_file;
                        }
                        $id = getId();
                        if(!empty($id)){
                           $_SESSION['id'] = $id+1;
                        }else{
                            $_SESSION['id'] = 19000000;
                        }
                    }
                }

                if (isset($_POST['submit'])) {
                    $check = true;
                    if (empty($_POST["studentName"])) {
                        echo "Hãy nhập tên.";
                        $check = false;
                        echo "<br>";
                    }
                    if (!isset($_POST["gender"])) {
                        echo "Hãy chọn giới tính.";
                        $check = false;
                        echo "<br>";
                    }
                    if ($_POST["department"] == "EMPTY") {
                        echo "Hãy chọn phân khoa.";
                        $check = false;
                        echo "<br>";
                    }
                    if (empty($_POST["birthday"])) {
                        $check = false;
                        echo "Hãy nhập ngày sinh.";
                        echo "<br>";
                    } else {
                        if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/", $_POST["birthday"])) {
                            echo "Hãy nhập ngày sinh đúng định dạng.";
                            echo "<br>";
                            $check = false;
                        }
                    }

                    if (!is_image()) {
                        $check = false;
                    }
                    if ($check) {
                        uploadImage();
                        header('location: confirm.php');
                    }
                }

                ?>


            </label>
        </div>
        <form class='form-input' method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data">
            <div class="content">
                <div class='lb'>
                    <label class="label-star">Họ và tên </label>
                </div>
                <input type='text' class='input-item' name="studentName">
            </div>
            <div class="content">
                <div class="lb">
                    <label class="label-star">Giới tính</label>
                </div>
                <div id="radio-content">
                    <?php
                    for ($x = 0; $x < count($genderArray); $x++) {
                        echo ("
                             <input type=\"radio\" value=\"$x\"  name=\"gender\">$genderArray[$x]</input>
                        ");
                    }

                    ?>
                </div>

            </div>
            <div class="content">
                <div class="lb">
                    <label class="label-star">Phân khoa</label>
                </div>
                <select name="department" class="select-item" name="department">
                    <?php

                    foreach ($departmentArray as $key => $value) {
                        echo ("
                            <option value = \"$key\">$value</option>
                        ");
                    }
                    ?>
                </select>
            </div>
            <div class="content">
                <div class='lb'>
                    <label class="label-star">Ngày sinh</label>
                </div>
                <input type='text' name="birthday" class='input-address' placeholder="dd/mm/yyyy" id="datepicker">

            </div>
            <div class="content">
                <div class='lb'>
                    <label>Địa chỉ </label>
                </div>
                <input type='text' class='input-item' name="address">
            </div>

            <div class="content">
                <div class='lb'>
                    <label>Hình ảnh</label>
                </div>
                <input type='file' class='input-file' name="image">
            </div>

            <input type='submit' value='Đăng ký' id='btn-submit' name="submit">
        </form>


    </div>


</body>

</html>